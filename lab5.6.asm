       .data 0x10000000
user1: .word 0
msg1:  .asciiz "Please enter an integer number:"
msg2:  .asciiz "If bytes were layed in reverse order the number would be:"


       .text
       .globl main
          
main: 
       add $sp, $sp, -4            # substract 4 form stack pointer
       sw $ra, 4($sp)              # save $ra in $sp
       la $a0, msg1
       li $v0, 4
       syscall
      
       li $v0, 5
       syscall 
         
       sw $v0, user1
       la $a0, user1
       jal Reverse_bytes
       la $a0, msg2
       li $v0, 4                   # print message
       syscall

       lw $t5, user1
       li $v0, 1                   # print number
       move $a0, $t5
       syscall
                       
       lw $ra, 4($sp)              # restore the returns address in $ra
       add $sp, $sp, 4             # free stack space
       jr $ra                      # return from main
   
Reverse_bytes: 
       lb $t1, 0($a0)              # swap highest and lowest byte
       lb $t2, 1($a0)
       lb $t3, 2($a0)
       lb $t4, 3($a0)
       sb $t4, 0($a0)
       sb $t3, 1($a0)
       sb $t2, 2($a0)
       sb $t1, 3($a0)
       jr $ra